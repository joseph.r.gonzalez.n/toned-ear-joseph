﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pitches;

public class Menu_Variables : MonoBehaviour
{
    public string speed;
    public string instrument;
    public int distance;
    public int pitch;
    public int correct_answers;
    public int total_questions;
    [HideInInspector] public List<Error_Pitch> error_pitches;
    [HideInInspector] public List<Pitch> current_notes;
}
