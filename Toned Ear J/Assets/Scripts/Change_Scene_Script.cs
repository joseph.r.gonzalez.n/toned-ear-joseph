﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Change_Scene_Script : MonoBehaviour
{
    public void to_home(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }
    public void to_play(){
        GameObject.Find("Menu_Manager").GetComponent<Menu_Script>().save_menu_variables();
        DontDestroyOnLoad (GameObject.Find("Menu_Variables"));
        SceneManager.LoadScene(1);
    }
    public void to_score(){
        int correct_answers = GameObject.Find("Manager").GetComponent<Check_Answer_Script>().correct_answers;
        int answered_questions = GameObject.Find("Manager").GetComponent<Check_Answer_Script>().answered_questions;
        GameObject.Find("Menu_Variables").GetComponent<Menu_Variables>().correct_answers = correct_answers;
        GameObject.Find("Menu_Variables").GetComponent<Menu_Variables>().total_questions = answered_questions;
        GameObject.Find("Menu_Variables").GetComponent<Menu_Variables>().current_notes = GetComponent<Pitches_Script>().current_notes;
        SceneManager.LoadScene(2);
    }
    public void to_error_scene(){
        SceneManager.LoadScene(3);
    }
}
