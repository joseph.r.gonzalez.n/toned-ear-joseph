﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Pitches;

public class Check_Answer_Script : MonoBehaviour{
    [HideInInspector] public AudioSource[] rdm_sources;
    [HideInInspector] public int current_iterator = 0;
    [HideInInspector] public bool is_button_pitch_available = false;
    [HideInInspector] public string current_pitch;
    public Color green = new Color(35/255f, 232/255f, 23/255f, 213/255f);  //(r, g, b, a)
    public Color red = new Color(232/255f, 34/255f, 23/255f, 217/255f);  //(r, g, b, a)
    public Sprite image_iterator;
    public Sprite image_normal;
    public int questions = 50;
    private int number_pitches;
    [HideInInspector] public bool correct_anwer = true;
    [HideInInspector] public bool is_completely_right = true;
    [HideInInspector] public int correct_answers = 0;
    [HideInInspector] public int answered_questions = 0;
    [HideInInspector] public Text text_score;
    [HideInInspector] public Image current_image_pitch_button;
    [HideInInspector] Dictionary<string, Pitch> notes;
    [HideInInspector] public List<Error_Pitch> error_pitches;
    [HideInInspector] public bool scene_number;

    void Start() {
        number_pitches = GameObject.Find("Menu_Variables").GetComponent<Menu_Variables>().pitch;
        error_pitches = GameObject.Find("Menu_Variables").GetComponent<Menu_Variables>().error_pitches;
        text_score = GameObject.Find("text_score").GetComponent<Text>();
        notes = GameObject.Find("Manager").GetComponent<General_Pitch>().notes;
    }
    public void Check_Answer_Note(){
        if(is_button_pitch_available && rdm_sources.Length>0){
            string real_note_raw = rdm_sources[current_iterator].clip.name;
            string real_note = real_note_raw.Split('_')[1];
            if(real_note.Equals(current_pitch)){
                current_image_pitch_button.color = green;
                GetComponent<Pitches_Script>().normal_color_pitches_button();
                Color color = correct_anwer? green : red;
                GetComponent<Space_Script>().Change_Space(image_normal,color,current_pitch);
                current_iterator += 1;
                correct_anwer = true;
                if(current_iterator < number_pitches){
                    GetComponent<Space_Script>().Change_Space(image_iterator,Color.clear,null);
                }else{
                    GetComponent<Buttons_Script>().pulse_next_finish = true;
                    if(is_completely_right)
                        correct_answers++;
                    answered_questions++;
                    text_score.text = "score:" + correct_answers.ToString() + "/" + answered_questions.ToString() + " total: " + questions.ToString();
                    is_completely_right = true;
                    if(answered_questions == questions)
                        GameObject.Find("Manager").GetComponent<Change_Scene_Script>().to_score();
                }
            }else{
                if(scene_number)
                    Filling_error_list();
                GetComponent<Space_Script>().Change_Space(null,red,current_pitch);
                current_image_pitch_button.color = red;
                correct_anwer = false;
                is_completely_right = false;
            }
            is_button_pitch_available = false;
        }
    }
    public void Filling_error_list(){
        string real_note_raw = rdm_sources[current_iterator].clip.name;
        int octeto = int.Parse(real_note_raw.Split('_')[0]);
        string real_note = real_note_raw.Split('_')[1];
        int identifier = (octeto-2)*12 + notes[real_note].index-1;
        bool found = false;
        bool search = true;
        int i = 0;
        while(search && i<error_pitches.Count){
            if(identifier == error_pitches[i].global_id){
                found = true;
                search = false;
            }
            if(identifier<error_pitches[i].global_id){
                search = false;
            }
            i++;
        }
        if(found){
            error_pitches[i-1].number_of_errors++;
        }else{
            if(!search){
                error_pitches.Insert(i-1,new Error_Pitch(real_note_raw,identifier,1));
            }else{
                error_pitches.Insert(i,new Error_Pitch(real_note_raw,identifier,1));
            }
        }
    }
}
