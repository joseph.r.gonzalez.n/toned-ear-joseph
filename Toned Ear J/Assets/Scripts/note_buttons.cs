﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class note_buttons : MonoBehaviour
{
    [HideInInspector] public Button note_button;
    [HideInInspector] public GameObject manager;
    [HideInInspector] public string current_pitch;
    public void send_pressed_note(){
        manager = GameObject.Find("Manager");
        note_button = GetComponent<Button>();
        current_pitch = note_button.name;
        manager.GetComponent<Check_Answer_Script>().current_pitch = current_pitch;
        manager.GetComponent<Check_Answer_Script>().is_button_pitch_available = true;
    }
}
