﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Instrument_Items : MonoBehaviour
{
    [HideInInspector] public GameObject menu;
    [HideInInspector] public bool activated_by_hand = false;
    private bool cicle_1 = false;

    public void on_select_instrument_item(){
        if(GetComponent<Toggle>().isOn && !cicle_1){
            menu = GameObject.Find("Menu_Manager");
            menu.GetComponent<Menu_Script>().instrument_toggle.gameObject.GetComponent<Instrument_Items>().activated_by_hand = false;
            menu.GetComponent<Menu_Script>().instrument_toggle.isOn = false;
            activated_by_hand = true;
            menu.GetComponent<Menu_Script>().instrument_toggle = GetComponent<Toggle>();
        }else{
            if(activated_by_hand){
                cicle_1 = true;
                GetComponent<Toggle>().isOn = true;
            }
        }
        cicle_1 = false;
    }
}
