﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pitches;

namespace Pitches{
    public class General_Pitch : MonoBehaviour{
        // Start is called before the first frame update
        public Dictionary<string, Pitch> notes = new Dictionary<string, Pitch>();
        void Start(){
            notes.Add("do", new Pitch("do","do",1));
            notes.Add("dos", new Pitch("dos","do #",2));
            notes.Add("re", new Pitch("re","re",3));
            notes.Add("res", new Pitch("res","re #",4));
            notes.Add("mi", new Pitch("mi","mi",5));
            notes.Add("fa", new Pitch("fa","fa",6));
            notes.Add("fas", new Pitch("fas","fa #",7));
            notes.Add("sol", new Pitch("sol","sol",8));
            notes.Add("sols", new Pitch("sols","sol #",9));
            notes.Add("la", new Pitch("la","la",10));
            notes.Add("las", new Pitch("las","la #",11));
            notes.Add("si", new Pitch("si","si",12));
        }
    }
}

