﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pitch_button : MonoBehaviour
{
    [HideInInspector] public Button note_button;
    [HideInInspector] public GameObject manager;
    [HideInInspector] public string current_pitch;
    public void send_pressed_note(){
        manager = GameObject.Find("Manager");
        note_button = GetComponent<Button>();
        current_pitch = note_button.name;
        manager.GetComponent<Keyboard_Script>().set_current_pitch(current_pitch);
    }
}
