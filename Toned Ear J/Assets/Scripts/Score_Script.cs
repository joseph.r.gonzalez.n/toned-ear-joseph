﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Pitches;

public class Score_Script : MonoBehaviour
{
    [HideInInspector] public int correct_answers;
    [HideInInspector] public int total_questions;
    [HideInInspector] public List<Error_Pitch> error_pitches;
    [HideInInspector] public int total_errors;
    void Start(){
        correct_answers = GameObject.Find("Menu_Variables").GetComponent<Menu_Variables>().correct_answers;
        total_questions = GameObject.Find("Menu_Variables").GetComponent<Menu_Variables>().total_questions;
        error_pitches = GameObject.Find("Menu_Variables").GetComponent<Menu_Variables>().error_pitches;
        string score = correct_answers.ToString() + "/" + total_questions.ToString();
        GameObject.Find("score").GetComponent<Text>().text = score;
        total_errors = 0;
        string errors = "";
        foreach (Error_Pitch error in error_pitches){
            errors = errors + error.number_of_errors.ToString() + "x" + error.note_name + "  ";
            total_errors += error.number_of_errors;
        }
        GameObject.Find("score_2").GetComponent<Text>().text = "errors: " + total_errors.ToString();
        GameObject.Find("errors").GetComponent<Text>().text = errors;
    }
}
