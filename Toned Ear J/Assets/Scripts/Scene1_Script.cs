﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Pitches;

public class Scene1_Script : MonoBehaviour
{
    public Sprite image_iterator;
    bool scene_1 = true;
    void Start(){ 
        GetComponent<Space_Script>().Place_Spaces();
        GetComponent<Space_Script>().Change_Space(image_iterator,Color.clear,null);
        GetComponent<Keyboard_Script>().scene_number = scene_1;
        GetComponent<Check_Answer_Script>().scene_number = scene_1;
        GetComponent<Pitches_Script>().scene_number = scene_1;
        GetComponent<Buttons_Script>().scene_number = scene_1;
    }
    void Update(){
        GetComponent<Keyboard_Script>().listen_keyboard();  
        GetComponent<Check_Answer_Script>().Check_Answer_Note();
        GetComponent<Listen_Script>().Update_Audio();
    }
}