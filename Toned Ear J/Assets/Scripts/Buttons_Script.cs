﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Buttons_Script : MonoBehaviour
{
    public bool pulse_next_finish = true;
    public bool previous_pulse_next_finish = true;
    private GameObject pitches_container;
    [HideInInspector] public Button listen_next;
    [HideInInspector] public Button listen_again;
    private Image listen_next_image;
    private Image listen_again_image;
    public Color purple = new Color(191/255f, 12/255f, 227/255f, 213/255f);  //(r, g, b, a)
    public Color blue = new Color(3/255f, 107/255f, 195/255f, 213/255f);  //(r, g, b, a)
    public Color white = new Color(194/255f, 194/255f, 194/255f, 255/255f);    //(r, g, b, a)
    [HideInInspector] public Color current_color;
    [HideInInspector] public bool scene_number;
    void Start() {
        pitches_container = GameObject.Find("Pitches_Container");
        listen_next = GameObject.Find("Listen_Container/listen_next").gameObject.GetComponent<Button>();
        listen_next_image = GameObject.Find("Listen_Container/listen_next").gameObject.GetComponent<Image>();
        listen_again = GameObject.Find("Listen_Container/listen_again").gameObject.GetComponent<Button>();
        listen_again_image = GameObject.Find("Listen_Container/listen_again").gameObject.GetComponent<Image>();
    }
    void Update() {
        current_color = scene_number? blue : purple;
        if(pulse_next_finish){
            listen_next.interactable = GetComponent<Pitches_Script>().current_notes.Count > 0;
            listen_next_image.color = GetComponent<Pitches_Script>().current_notes.Count > 0 ? current_color : white;
        }
        if(previous_pulse_next_finish==false && pulse_next_finish==true){
            activate_pitches(false);
            previous_pulse_next_finish = !previous_pulse_next_finish;
        }
        if(previous_pulse_next_finish==true && pulse_next_finish==false){
            listen_again.interactable = true;
            listen_next.interactable = false;
            listen_again_image.color = current_color;
            listen_next_image.color = white;
            activate_pitches(true);
            previous_pulse_next_finish = !previous_pulse_next_finish;
        }
    }
    void activate_pitches(bool activate){
        for(int i=0; i<pitches_container.transform.childCount; i++){
            Button pitch_button = pitches_container.transform.GetChild(i).gameObject.GetComponent<Button>();
            pitch_button.interactable = activate;   
        }
    }
}
