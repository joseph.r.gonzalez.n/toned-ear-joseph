﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Pitches;

public class Pitches_Script : MonoBehaviour{
    public GameObject pitches_container;
    public GameObject pitches_prefab;
    public bool is_for_computer = true;
    public int space_distance;
    public int vertical_distance;
    public Vector3 center_position;
    public bool note_changed = false;
    public List<Pitch> current_notes = new List<Pitch>();
    public Color blue = new Color(3/255f, 107/255f, 195/255f, 213/255f);  //(r, g, b, a)
    public Color purple = new Color(191/255f, 12/255f, 227/255f, 213/255f);  //(r, g, b, a)
    public Color current_color;
    [HideInInspector] public bool scene_number;
    float scale_of_pitch_button;
    
    void Start(){
        if(is_for_computer){
            scale_of_pitch_button = 0.3f;
            space_distance = 35;
            vertical_distance = -25;
            center_position = new Vector3(0F, -90F, 0F);
        }else{
            scale_of_pitch_button = 1f;
            space_distance = 95;
            vertical_distance = -45;
            center_position = new Vector3(0F, 50F, 0F);
        }
    }
    public void Control_Notes(){
        if(note_changed){
            for(int i=0; i<pitches_container.transform.childCount; i++){
                Destroy(pitches_container.transform.GetChild(i).gameObject);
            } 
            int number_pitches = current_notes.Count;
            int number_pitches1;
            int number_pitches2 = 0;
            if(number_pitches>6){
                number_pitches1 = 6;
                number_pitches2 = number_pitches - 6;
            }else{
                number_pitches1 = number_pitches;
            }
            int start_point = space_distance*(number_pitches1-1)/2;
            for (int i = 0; i < number_pitches1; i++){
                GameObject new_space = Instantiate(pitches_prefab);
                new_space.transform.SetParent(pitches_container.transform);
                new_space.transform.localScale = new Vector3(scale_of_pitch_button, scale_of_pitch_button, 1);
                Vector3 temp = center_position;
                temp.x = temp.x - start_point + i*space_distance;
                new_space.transform.localPosition = temp;
                new_space.name = current_notes[i].inter_name;
                new_space.GetComponentInChildren<Text>().text = current_notes[i].real_name;
            }
            start_point = space_distance*(number_pitches2-1)/2;
            for (int i = 0; i < number_pitches2; i++){
                GameObject new_space = Instantiate(pitches_prefab);
                new_space.transform.SetParent(pitches_container.transform);
                new_space.transform.localScale = new Vector3(scale_of_pitch_button, scale_of_pitch_button, 1);
                Vector3 temp = center_position;
                temp.x = temp.x - start_point + i*space_distance;
                temp.y += vertical_distance; 
                new_space.transform.localPosition = temp;
                new_space.name = current_notes[6 + i].inter_name;
                new_space.GetComponentInChildren<Text>().text = current_notes[6+i].real_name;
            }
            note_changed = false;
        }
    }
    public void normal_color_pitches_button(){
        current_color = scene_number? blue : purple;
        for(int i=0; i<pitches_container.transform.childCount; i++){
            pitches_container.transform.GetChild(i).gameObject.GetComponent<Image>().color = current_color;
        }
    }
}
