﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fill_Audio : MonoBehaviour{
    [HideInInspector] public string[] names = new string[]{
        "2_do", "2_dos", "2_re", "2_res", "2_mi", "2_fa", "2_fas", "2_sol", "2_sols", "2_la", "2_las", "2_si",
        "3_do", "3_dos", "3_re", "3_res", "3_mi", "3_fa", "3_fas", "3_sol", "3_sols", "3_la", "3_las", "3_si",
        "4_do", "4_dos", "4_re", "4_res", "4_mi", "4_fa", "4_fas", "4_sol", "4_sols", "4_la", "4_las", "4_si",
        "5_do", "5_dos", "5_re", "5_res", "5_mi", "5_fa", "5_fas", "5_sol", "5_sols", "5_la", "5_las", "5_si",
        "6_do", "6_dos", "6_re", "6_res", "6_mi", "6_fa", "6_fas", "6_sol", "6_sols", "6_la", "6_las", "6_si",
        "7_do", "7_dos", "7_re", "7_res", "7_mi", "7_fa", "7_fas", "7_sol", "7_sols", "7_la", "7_las", "7_si",
        "8_do", "8_dos", "8_re", "8_res", "8_mi", "8_fa", "8_fas", "8_sol", "8_sols", "8_la", "8_las", "8_si"
        };
    void Awake() {
        string speed = GameObject.Find("Menu_Variables").GetComponent<Menu_Variables>().speed;
        string instrument = GameObject.Find("Menu_Variables").GetComponent<Menu_Variables>().instrument;
        for(int i=0; i <names.Length; i++){
            AudioSource audio = gameObject.AddComponent<AudioSource>();
            audio.clip =  Resources.Load<AudioClip>("Sounds/" + instrument + "/" + speed + "/" + names[i]);
            audio.playOnAwake = false;
        }
    }
}
