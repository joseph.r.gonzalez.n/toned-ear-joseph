﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Space_Script : MonoBehaviour
{
    public GameObject space_container;
    public GameObject space_prefab;
    public Sprite image_iterator;
    public Sprite image_normal;
    public Color blue = new Color(3/255f, 107/255f, 195/255f, 213/255f);  //(r, g, b, a)
    public Color normal = new Color(194/255f, 194/255f, 194/255f, 255/255f);    //(r, g, b, a)
    public bool is_for_computer = true;
    [HideInInspector] private int current_iterator;
    private int number_pitches;
    [HideInInspector] public int space_distance;
    [HideInInspector] public Vector3 center_position;
    [HideInInspector] public int vertical_distance;
    [HideInInspector] float scale_space_button;
    
    void Awake(){
        number_pitches = GameObject.Find("Menu_Variables").GetComponent<Menu_Variables>().pitch;
        if(is_for_computer){
            vertical_distance = -35;
            space_distance = 50;
            center_position = new Vector3(-55F, 5F, 0F);
            scale_space_button = 0.55f;
        }else{
            vertical_distance = -65;
            space_distance = 100;
            center_position = new Vector3(-85F, 400F, 0F);
            scale_space_button = 1f;
        }
    }
    
    public void Place_Spaces(){
        int number_pitches1;
        int number_pitches2 = 0;
        if(number_pitches>6){
            number_pitches1 = 6;
            number_pitches2 = number_pitches - 6;
        }else{
            number_pitches1 = number_pitches;
        }
        int start_point = space_distance*(number_pitches1-1)/2;
        for (int i = 0; i < number_pitches1; i++){
            GameObject new_space = Instantiate(space_prefab);
            new_space.transform.parent = space_container.transform;
            new_space.transform.localScale = new Vector3(scale_space_button, scale_space_button, 1);
            Vector3 temp = center_position;
            temp.x = temp.x - start_point + i*space_distance;
            new_space.transform.localPosition = temp;
            new_space.name = "Space";
        }
        start_point = space_distance*(number_pitches2-1)/2;
        for (int i = 0; i < number_pitches2; i++){
            GameObject new_space = Instantiate(space_prefab);
            new_space.transform.parent = space_container.transform;
            new_space.transform.localScale = new Vector3(scale_space_button, scale_space_button, 1);
            Vector3 temp = center_position;
            temp.x = temp.x - start_point + i*space_distance;
            temp.y += vertical_distance; 
            new_space.transform.localPosition = temp;
            new_space.name = "Space";
        }
    }
    public void Clear_Spaces(){
        for(int i=0; i<space_container.transform.childCount; i++){
            current_iterator = i;
            GetComponent<Check_Answer_Script>().current_iterator = current_iterator;
            if(i==0){
                Change_Space(image_iterator,normal,"");
            }else{
                Change_Space(image_normal,normal,"");
            }
        }
        GetComponent<Check_Answer_Script>().current_iterator = 0;
    }
    public void Change_Space(Sprite image, Color color, string text){
        current_iterator = GetComponent<Check_Answer_Script>().current_iterator;
        Transform space = space_container.gameObject.transform.GetChild(current_iterator);
        GameObject space_image = space.Find("Image").gameObject;
        if(image != null)
            space_image.GetComponent<Image>().sprite = image;       
        if(!color.Equals(Color.clear))
            space_image.GetComponent<Image>().color = color;
        GameObject space_text = space.Find("Text").gameObject;
        if(text != null)
            space_text.GetComponent<Text>().text = text;
    }

}
