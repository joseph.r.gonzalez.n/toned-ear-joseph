﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class iterator_sound_script : MonoBehaviour
{
    [HideInInspector] public int global_id_thread = 1;
    [HideInInspector] public bool is_thread_running = false;
    [HideInInspector] public int killed_thread = -1;
    [HideInInspector] public bool kill_thread = false;
    [HideInInspector] public AudioSource[] rdm_sources;
    [HideInInspector] public bool scene_number;     // initialized by error_script and scene1_script
    private int number_pitches;
    private string speed;
    void Start() {
        number_pitches = GameObject.Find("Menu_Variables").GetComponent<Menu_Variables>().pitch;
        rdm_sources = new AudioSource[number_pitches];
        speed = GameObject.Find("Menu_Variables").GetComponent<Menu_Variables>().speed;  
    }
    public IEnumerator Iterator_Sounds(){
        int id_thread = global_id_thread;
        global_id_thread = (global_id_thread + 1) % 10;
        // correct_answer.text = id_thread.ToString();
        if(is_thread_running){
            if(id_thread==0){
                killed_thread = 9;
            }else{
                killed_thread = (id_thread - 1) % 10;
            }
            kill_thread = true;   
        }
        foreach (AudioSource audio in rdm_sources){
            is_thread_running = true;
            if(kill_thread && (killed_thread==id_thread)){
                kill_thread = false;
                killed_thread = -1;
                // deb.text = "kill: " + id_thread.ToString();
                break;
            }
            // correct_answer.text = audio.clip.name;
            audio.Play();
            if(speed.Equals("slow")){
                yield return new WaitForSeconds(1f); 
            }else if(speed.Equals("fast")){
                yield return new WaitForSeconds(0.5f); 
            }
        }
        is_thread_running = false;
    }
}
