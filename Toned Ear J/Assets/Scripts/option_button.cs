﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Pitches;

public class option_button : MonoBehaviour
{
    [HideInInspector] public GameObject manager;
    [HideInInspector] public Color blue = new Color(3/255f, 107/255f, 195/255f, 213/255f);  //(r, g, b, a)
    [HideInInspector] public Color green = new Color(35/255f, 232/255f, 23/255f, 213/255f);  //(r, g, b, a)
    [HideInInspector] private Color white = new Color(255/255f, 255/255f, 255/255f, 255/255f);  //(r, g, b, a)
    [HideInInspector] public bool isSelected = false;
    private List<Pitch> current_notes;
    Dictionary<string, Pitch> notes;

    public void onClickPitchButton(){
        manager = GameObject.Find("Manager");
        current_notes = manager.GetComponent<Pitches_Script>().current_notes;
        isSelected = !isSelected;
        Color color = isSelected? blue : white;
        GetComponent<Image>().color = color;
        notes = manager.GetComponent<General_Pitch>().notes;
        if(isSelected){
            addNote();
        }else{
            current_notes.Remove(notes[gameObject.name]);
        }
        manager.GetComponent<Pitches_Script>().note_changed = true;
        manager.GetComponent<Listen_Script>().current_notes_changed = true;
    }

    private void addNote(){
        int new_index = notes[gameObject.name].index;
        bool found = false;
        int i = 0;
        while(!found && i<current_notes.Count){
            if(new_index<current_notes[i].index){
                found = true;
            }
            i++;
        }
        if(!found){
            current_notes.Insert(current_notes.Count,notes[gameObject.name]);
        }else{
            current_notes.Insert(i-1,notes[gameObject.name]);
        } 
    }
}
