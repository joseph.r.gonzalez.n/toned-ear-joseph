﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Speed_Items : MonoBehaviour
{
    [HideInInspector] public GameObject menu;
    [HideInInspector] public bool activated_by_hand = false;
    private bool cicle_1 = false;

    public void on_select_speed_item(){
        if(GetComponent<Toggle>().isOn && !cicle_1){
            menu = GameObject.Find("Menu_Manager");
            menu.GetComponent<Menu_Script>().speed_toggle.gameObject.GetComponent<Speed_Items>().activated_by_hand = false;
            menu.GetComponent<Menu_Script>().speed_toggle.isOn = false;
            activated_by_hand = true;
            menu.GetComponent<Menu_Script>().speed_toggle = GetComponent<Toggle>();
        }else{
            if(activated_by_hand){
                cicle_1 = true;
                GetComponent<Toggle>().isOn = true;
            }
        }
        cicle_1 = false;
    }
}
