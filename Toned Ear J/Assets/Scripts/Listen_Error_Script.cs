﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pitches;

public class Listen_Error_Script : MonoBehaviour
{
    public List<Pitch> current_notes;
    [HideInInspector] public GameObject audio_source;
    [HideInInspector] public AudioSource[] sources = new AudioSource[12*7];
    [HideInInspector] public AudioSource[] rdm_sources;
    public List<AudioSource> selected_sources;
    [HideInInspector] public List<Error_Pitch> error_pitches;
    [HideInInspector] public bool is_new_source = false;
    private int octetos = 7;
    private int number_pitches;
    void Start(){
        audio_source = GameObject.Find("Audio");
        sources = audio_source.GetComponents<AudioSource>();
        number_pitches = GameObject.Find("Menu_Variables").GetComponent<Menu_Variables>().pitch;
        rdm_sources = new AudioSource[number_pitches];
    }
    public void HearNext(){
        GetComponent<Space_Script>().Clear_Spaces();
        for(int i=0; i<number_pitches; i++)
            rdm_sources[i] = selected_sources[Random.Range(0, selected_sources.Count - 1)];
        is_new_source = true;
        GetComponent<Buttons_Script>().pulse_next_finish = false;
        GetComponent<iterator_sound_script>().rdm_sources = rdm_sources;
        StartCoroutine(GetComponent<iterator_sound_script>().Iterator_Sounds());
    }
    public void HearAgain(){
        GetComponent<iterator_sound_script>().rdm_sources = rdm_sources;
        StartCoroutine(GetComponent<iterator_sound_script>().Iterator_Sounds());
    }
    public void Fill_Audio_Source(){
        current_notes = GetComponent<Error_Script>().current_notes;
        for(int i=0; i<octetos; i++){
            for(int j=0; j<current_notes.Count; j++){
                selected_sources.Add(sources[current_notes[j].index-1 + i*12]);
                // Debug.Log(sources[current_notes[j].index-1 + i*12].clip);
            }
        }
        error_pitches = GetComponent<Error_Script>().error_pitches;
        for(int i=0; i<error_pitches.Count; i++){
            for(int j=0; j<error_pitches[i].number_of_errors; j++){
                selected_sources.Add(sources[error_pitches[i].global_id]);
                // Debug.Log(sources[error_pitches[i].global_id].clip);
            }     
        }
    }
    public void Update_Audio(){
        if(is_new_source){
            GetComponent<Check_Answer_Script>().rdm_sources = rdm_sources;
            is_new_source = false;
        }
    }
}
