﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Pitches;


public class Menu_Script : MonoBehaviour
{
    [HideInInspector] public Toggle speed_toggle;
    [HideInInspector] public Toggle instrument_toggle;
    [HideInInspector] public Slider distance_slider;
    [HideInInspector] public Slider pitch_slider;
    [HideInInspector] public Text distance_text;
    [HideInInspector] public Text pitch_text;
    [HideInInspector] public GameObject menu_variables;
    
    void Start(){
        speed_toggle = GameObject.Find("slow_item").GetComponent<Toggle>();
        GameObject.Find("slow_item").GetComponent<Speed_Items>().activated_by_hand = true;
        instrument_toggle = GameObject.Find("piano_item").GetComponent<Toggle>();
        GameObject.Find("piano_item").GetComponent<Instrument_Items>().activated_by_hand = true;
        distance_slider = GameObject.Find("distance_slider").GetComponent<Slider>();
        pitch_slider = GameObject.Find("pitch_slider").GetComponent<Slider>();
        distance_text = GameObject.Find("distance_slot").GetComponent<Text>();
        pitch_text = GameObject.Find("pitch_slot").GetComponent<Text>();
        distance_slider.value = 7;
        menu_variables = GameObject.Find("Menu_Variables");
        menu_variables.GetComponent<Menu_Variables>().error_pitches = new List<Error_Pitch>();
    }
    void Update(){
        distance_text.text = distance_slider.value.ToString();
        pitch_text.text = pitch_slider.value.ToString();
    }
    public void save_menu_variables(){
        menu_variables.GetComponent<Menu_Variables>().speed = speed_toggle.gameObject.name.Split('_')[0];
        menu_variables.GetComponent<Menu_Variables>().instrument = instrument_toggle.gameObject.name.Split('_')[0];
        menu_variables.GetComponent<Menu_Variables>().distance = Mathf.RoundToInt(distance_slider.value);
        menu_variables.GetComponent<Menu_Variables>().pitch = Mathf.RoundToInt(pitch_slider.value);
    }  
}
