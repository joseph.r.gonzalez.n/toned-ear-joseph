﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Keyboard_Script : MonoBehaviour
{
    [HideInInspector] public Button button_listen_next;
    [HideInInspector] public Button button_listen_again;
    private Transform pitches_container_transform;
    private MonoBehaviour listen_script_object;
    [HideInInspector] public bool scene_number;     // initialized by error_script and scene1_script

    void Start() {
        button_listen_next = GameObject.Find("Listen_Container").transform.Find("listen_next").gameObject.GetComponent<Button>();
        button_listen_again = GameObject.Find("Listen_Container").transform.Find("listen_again").gameObject.GetComponent<Button>();
        pitches_container_transform = GameObject.Find("Pitches_Container").transform;
    }
    public void listen_keyboard(){
        if(Input.anyKeyDown){
            if (Input.GetKeyDown(KeyCode.Space) && button_listen_next.interactable){
                if(scene_number){
                    GetComponent<Listen_Script>().HearNext();
                }else{
                    GetComponent<Listen_Error_Script>().HearNext();
                }
            }
            if (Input.GetKeyDown(KeyCode.M) && button_listen_again.interactable){
                if(scene_number){
                    GetComponent<Listen_Script>().HearAgain();
                }else{
                    GetComponent<Listen_Error_Script>().HearAgain();
                }
            }
            if(Input.GetKeyDown(KeyCode.Alpha1))
                check_button_availability("do");
            if(Input.GetKeyDown(KeyCode.Alpha2))
                check_button_availability("re");
            if(Input.GetKeyDown(KeyCode.Alpha3))
                check_button_availability("mi");
            if(Input.GetKeyDown(KeyCode.Alpha4))
                check_button_availability("fa");
            if(Input.GetKeyDown(KeyCode.Alpha5))
                check_button_availability("sol");
            if(Input.GetKeyDown(KeyCode.Alpha6))
                check_button_availability("la");
            if(Input.GetKeyDown(KeyCode.Alpha7))
                check_button_availability("si");
            if(Input.GetKeyDown(KeyCode.Y))
                check_button_availability("dos");
            if(Input.GetKeyDown(KeyCode.U))
                check_button_availability("res");
            if(Input.GetKeyDown(KeyCode.I))
                check_button_availability("fas");
            if(Input.GetKeyDown(KeyCode.O))
                check_button_availability("sols");
            if(Input.GetKeyDown(KeyCode.P))
                check_button_availability("las");
        }   
    }
    void check_button_availability(string name_note){
        Transform pitch_button = pitches_container_transform.Find(name_note);
        bool is_interactable = false;
        if(pitch_button != null){
            is_interactable = pitch_button.gameObject.GetComponent<Button>().interactable;
        }
        if(is_interactable)
            set_current_pitch(name_note);
    }
    public void set_current_pitch(string current_Pitch){
        GetComponent<Check_Answer_Script>().current_pitch = current_Pitch;
        GetComponent<Check_Answer_Script>().is_button_pitch_available = true;
        GetComponent<Check_Answer_Script>().current_image_pitch_button = pitches_container_transform.Find(current_Pitch).gameObject.GetComponent<Image>();
    }
}
