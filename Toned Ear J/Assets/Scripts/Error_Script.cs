﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Pitches;

public class Error_Script : MonoBehaviour
{
    [HideInInspector] public List<Pitch> current_notes;
    [HideInInspector] public List<Error_Pitch> error_pitches;
    public Sprite image_iterator;
    bool scene_error = false;
    void Start(){
        current_notes = GameObject.Find("Menu_Variables").GetComponent<Menu_Variables>().current_notes;
        error_pitches = GameObject.Find("Menu_Variables").GetComponent<Menu_Variables>().error_pitches;
        GetComponent<Pitches_Script>().current_notes = current_notes;
        GetComponent<Pitches_Script>().note_changed = true;
        GetComponent<Pitches_Script>().Control_Notes();
        GetComponent<Listen_Error_Script>().Fill_Audio_Source();  
        GetComponent<Space_Script>().Place_Spaces();
        GetComponent<Space_Script>().Change_Space(image_iterator,Color.clear,null);
        GetComponent<Keyboard_Script>().scene_number = scene_error;
        GetComponent<Check_Answer_Script>().scene_number = scene_error;
        GetComponent<Pitches_Script>().scene_number = scene_error;
        GetComponent<Buttons_Script>().scene_number = scene_error;
        GetComponent<Pitches_Script>().normal_color_pitches_button();
    }
    void Update() {
        GetComponent<Keyboard_Script>().listen_keyboard();
        GetComponent<Check_Answer_Script>().Check_Answer_Note();
        GetComponent<Listen_Error_Script>().Update_Audio();  
    }
}
