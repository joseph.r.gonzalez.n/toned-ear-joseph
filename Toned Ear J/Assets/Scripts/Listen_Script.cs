﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pitches;
using UnityEngine.UI;

public class Listen_Script : MonoBehaviour{
    [HideInInspector] public AudioSource[] sources = new AudioSource[12*7];     //Aqui se cambia los octectos
    [HideInInspector] public List<AudioSource> selected_sources;
    [HideInInspector] public AudioSource[] rdm_sources;
    [HideInInspector] public List<Pitch> current_notes;
    [HideInInspector] public bool is_new_source = false;    //To talk to Space_Script method
    [HideInInspector] public GameObject audio_source;
    [HideInInspector] public bool current_notes_changed = false;
    private int octetos = 7;
    private int number_pitches;
    private int distance;
    private Text correct_answer;
    private Text deb;
    void Start(){
        number_pitches = GameObject.Find("Menu_Variables").GetComponent<Menu_Variables>().pitch;
        distance = GameObject.Find("Menu_Variables").GetComponent<Menu_Variables>().distance;
        // number_pitches = 4;
        audio_source = GameObject.Find("Audio");
        sources = audio_source.GetComponents<AudioSource>();
        rdm_sources = new AudioSource[number_pitches];
        current_notes = GetComponent<Pitches_Script>().current_notes;
        correct_answer = GameObject.Find("correct_answer").GetComponent<Text>();
        deb = GameObject.Find("deb").GetComponent<Text>();
    }
    public void HearNext(){
        Fill_Audio_Source();
        GetComponent<Space_Script>().Clear_Spaces();
        GetComponent<Pitches_Script>().Control_Notes();
        int center = Random.Range(0, octetos-1);
        int inicio = center - distance/2;
        if(inicio<0)
            inicio = 0;
        if(center + distance/2 > octetos-1)
            inicio = octetos - distance;
        int start_point = inicio * current_notes.Count;
        int end_point = (inicio + distance) * current_notes.Count - 1;
        for (int i = 0; i < number_pitches; i++)
            rdm_sources[i] = selected_sources[Random.Range(start_point, end_point)];
        is_new_source = true;
        GetComponent<Buttons_Script>().pulse_next_finish = false;
        GetComponent<iterator_sound_script>().rdm_sources = rdm_sources;
        StartCoroutine(GetComponent<iterator_sound_script>().Iterator_Sounds());
    }
    public void HearAgain(){
        GetComponent<iterator_sound_script>().rdm_sources = rdm_sources;
        StartCoroutine(GetComponent<iterator_sound_script>().Iterator_Sounds());
    }
    void Fill_Audio_Source(){
        if(current_notes_changed){
            selected_sources.Clear();
            current_notes = GetComponent<Pitches_Script>().current_notes;
            for(int i=0; i<octetos; i++){
                for(int j=0; j<current_notes.Count; j++)
                    selected_sources.Add(sources[current_notes[j].index-1 + i*12]);
            }
            current_notes_changed = false;
        }
    }
    public void Update_Audio(){
        if(is_new_source){
            GetComponent<Check_Answer_Script>().rdm_sources = rdm_sources;
            is_new_source = false;
        }
    }
}
