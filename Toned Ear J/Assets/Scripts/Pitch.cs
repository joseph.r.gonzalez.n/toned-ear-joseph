﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pitches{
    public class Pitch{
        public string inter_name;
        public string real_name;
        public int index;
        public Pitch(string inter_name_input, string real_name_input, int index_input){
            inter_name = inter_name_input;
            real_name = real_name_input;
            index = index_input;
        }
    }
    public class Error_Pitch{
        public string note_name;
        public int global_id;
        public int number_of_errors;
        public Error_Pitch(string note_name_input, int global_id_input, int number_of_errors_input){
            note_name = note_name_input;
            global_id = global_id_input;
            number_of_errors = number_of_errors_input;
        }
    }
}
